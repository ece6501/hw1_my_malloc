#include "stdlib.h"
#include "stdio.h"
#include <unistd.h>

#include "my_malloc.h"

size_t segmentSize = 0;
size_t freeSpace = 0;

block* freeBlockHead = NULL;
block* freeBlockTail = NULL;

void* ff_malloc(size_t size) {
  block* freeBlock = findFirstFit(freeBlockHead, size);
  // If suitable block is found.
  if (freeBlock != NULL) {
    return reuseBlock(freeBlock, size);
  }
  return addBlock(size);
}

void ff_free(void* ptr) {
  if (ptr == NULL) { return; } // for large_size valgrind clean
  block* block_ = (block*) ptr - 1; // Back to the start of the block
  freeSpace += (sizeof(block) + block_->dataSize);
  putToFreeList(block_);
  if (block_->next != NULL) mergeFreeSpace(block_, block_->next);
  if (block_->prev != NULL) mergeFreeSpace(block_->prev, block_);
}

void* bf_malloc(size_t size) {
  block* freeBlock = findBestFit(freeBlockHead, size);
  // If suitable block is found.
  if (freeBlock != NULL) {
    return reuseBlock(freeBlock, size);
  }
  return addBlock(size);
}

void bf_free(void* ptr) {
  ff_free(ptr);
}

block* findFirstFit(block* curr, size_t size) {
  while (curr != NULL) {
    if (curr->dataSize >= size) {
      return curr;
    }
    curr = curr->next;
  }
  return curr;
}

block* findBestFit(block* curr, size_t size) {
  block* candidate = NULL;
  while (curr != NULL) {
    if (curr->dataSize == size) {
      candidate = curr;
      break;
    }
    if (curr->dataSize > size) {
      if (candidate == NULL) {
        candidate = curr;
      } else if (candidate->dataSize > curr->dataSize) {
        candidate = curr;
      }
    }
    curr = curr->next;
  }
  return candidate;
}

void* addBlock(size_t size) {
  size_t currSize = sizeof(block) + size;
  void* spacePtr = sbrk(currSize);
  if (spacePtr == (void*) -1) { return NULL; }
  block* block_ = (block*) spacePtr;
  segmentSize += currSize;
  block_->dataSize = size;
  block_->prev = NULL; block_->next = NULL;
  return block_ + 1;
}

void* reuseBlock(block* block_, size_t size) {
  /* If previous size is greater than curr required size + metadata (for the remain parts),
   * we need to split the free block.
   */
  if (block_->dataSize >= sizeof(block) + size) {
    splitBlock(block_, size);
    freeSpace -= (sizeof(block) + size);
  } else { // otherwise, simply remove from the list of free blocks, set the dataSize to the malloced size
    removeFreeBlock(block_);
    freeSpace -= (sizeof(block) + block_->dataSize);
  }
  return block_ + 1; // return the pointer to the start of the data
}

void splitBlock(block* block_, size_t size) {
  size_t currSize = sizeof(block) + size; // metadata + dataSize 
  block* remainFree = (block*) ((char*)(block_) + currSize); // start of remainFree
  remainFree->dataSize = block_->dataSize - currSize;
  block_->dataSize = size;
  removeAndLink(block_, remainFree);
}

void removeFreeBlock(block* block_) {
  if (block_ == freeBlockHead || block_ == freeBlockTail) {
    if (block_ == freeBlockHead) { // head
      freeBlockHead = block_->next;
      if (freeBlockHead != NULL) {
        freeBlockHead->prev = NULL;
      }
    }
    
    if (block_ == freeBlockTail) { // tail
      freeBlockTail = block_->prev;
      if (freeBlockTail != NULL) {
        freeBlockTail->next = NULL;
      }
    }
  } else { // middle
    block_->prev->next = block_->next;
    block_->next->prev = block_->prev;
  }
  block_->prev = NULL; block_->next = NULL;
}

void removeAndLink(block* block_, block* remainFree) {
  if (block_ == freeBlockHead || block_ == freeBlockTail) {
    if (block_ == freeBlockHead) { // head
      freeBlockHead = remainFree;
      freeBlockHead->prev = NULL;
      remainFree->next = block_->next;
      if (remainFree->next != NULL) {
        remainFree->next->prev = remainFree;
      }
    }
    if (block_ == freeBlockTail) { // tail
      freeBlockTail = remainFree;
      freeBlockTail->next = NULL;
      freeBlockTail->prev = block_->prev;
      if (freeBlockTail->prev != NULL) {
        freeBlockTail->prev->next = freeBlockTail;
      }
    }
  } else { // in the middle
    remainFree->prev = block_->prev;
    remainFree->next = block_->next;
    remainFree->prev->next = remainFree;
    remainFree->next->prev = remainFree;
  }
}

// Put according to the order of physical address.
void putToFreeList(block* block_) {
  block_->prev = NULL; block_->next = NULL;
  if (freeBlockHead == NULL && freeBlockTail == NULL) {
    freeBlockHead = freeBlockTail = block_;
  } else if (block_ < freeBlockHead) {
    block_->next = freeBlockHead;
    freeBlockHead->prev = block_;
    freeBlockHead = block_;
  } else if (block_ > freeBlockTail) {
    block_->prev = freeBlockTail;
    freeBlockTail->next = block_;
    freeBlockTail = block_;
  } else {
    block* curr = freeBlockHead;
    while (curr != NULL && block_ > curr->next) {
      curr = curr->next;
    }
    block_->next = curr->next;
    block_->prev = curr;
    curr->next = block_;
    block_->next->prev = block_;
  }
}

// Merge two blocks if they are ajacent.
void mergeFreeSpace(block* block_1, block* block_2) {
  char* segment = (char*) block_1 + sizeof(block) + block_1->dataSize;
  if (segment == (char*) block_2) {
    block_1->dataSize += sizeof(block) + block_2->dataSize;
    block_1->next = block_2->next;
    if (block_1->next == NULL) {
      freeBlockTail = block_1;
    } else {
      block_1->next->prev = block_1;
    }
    block_2->prev = NULL; block_2->next = NULL;
  }
}

unsigned long get_data_segment_size() {
  return segmentSize;
}

unsigned long get_data_segment_free_space_size() {
  return freeSpace;
}
