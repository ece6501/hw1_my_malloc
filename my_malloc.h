#ifndef __MY_MALLOC_H__
#define __MY_MALLOC_H__

#include <stdlib.h>

struct block_t {
   size_t dataSize;
   struct block_t* prev;
   struct block_t* next;
};
typedef struct block_t block;

void* ff_malloc(size_t size);
void ff_free(void* ptr);
void* bf_malloc(size_t size);
void bf_free(void* ptr);
unsigned long get_data_segment_size();
unsigned long get_data_segment_free_space_size();

block* findFirstFit(block* head, size_t size);
block* findBestFit(block* head, size_t size);
void* addBlock(size_t size);
void* reuseBlock(block* block_, size_t size);

void splitBlock(block* block_, size_t size);
void removeFreeBlock(block* block_);
void removeAndLink(block* block_, block* remainFree);
void putToFreeList(block* block_);
void mergeFreeSpace(block* block_1, block* block_2);

#endif
